import ROOT 

def convert_color(color, mode):
    """
    Convert *color* to a TColor if *mode='root'* or to (r,g,b) if 'mpl'.
 
    The *color* argument can be a ROOT TColor or color index, an *RGB*
    or *RGBA* sequence or a string in any of several forms:
 
        1) a letter from the set 'rgbcmykw'
        2) a hex color string, like '#00FFFF'
        3) a standard name, like 'aqua'
        4) a float, like '0.4', indicating gray on a 0-1 scale
 
    if *arg* is *RGBA*, the transparency value will be ignored.
    """
    mode = mode.lower()
    if mode not in ('mpl', 'root'):
        raise ValueError(
            "`{0}` is not a valid `mode`".format(mode))
    try:
        # color is an r,g,b tuple
        color = tuple([float(x) for x in color[:3]])
        if max(color) > 1.:
            color = tuple([x / 255. for x in color])
        if mode == 'root':
            #print color
            return ROOT.TColor.GetColor(*color)
        return color
    except (ValueError, TypeError):
        pass
    if isinstance(color, str):
        #if color in _cnames:
            # color is a matplotlib letter or an html color name
        #    color = _cnames[color]
        if color[0] == '#':
            # color is a hex value
            color = color.lstrip('#')
            lv = len(color)
            color = tuple(int(color[i:i + lv // 3], 16)
                          for i in range(0, lv, lv // 3))
            if lv == 3:
                color = tuple(x * 16 + x for x in color)
            return convert_color(color, mode)
        # color is a shade of gray, i.e. '0.3'
        return convert_color((color, color, color), mode)
    try:
        # color is a TColor
        color = ROOT.TColor(color)
        color = color.GetRed(), color.GetGreen(), color.GetBlue()
        return convert_color(color, mode)
    except (TypeError, ReferenceError):
        pass
    try:
        # color is a ROOT color index
        if color < 0:
            color = 0
        color = ROOT.gROOT.GetColor(color)
        # Protect against the case a histogram with a custom color
        # is saved in a ROOT file
        if not color:
            # Just return black
            color = ROOT.gROOT.GetColor(1)
        color = color.GetRed(), color.GetGreen(), color.GetBlue()
        return convert_color(color, mode)
    except (TypeError, ReferenceError):
        pass
    raise ValueError("'{0!s}' is not a valid `color`".format(color))

