#!/usr/bin/env python

#Importing useful packages

import ROOT
import numpy as np
import itertools
import os.path
import sys
import argparse
import BeekFunctions

parser = argparse.ArgumentParser(description="make plots")
parser.add_argument("-f","--OutputFolder",type=str,action='store', default = "",help="fill in")
parser.add_argument("-n","--OutputName",type=str,action='store', default = "", help="fill in")
parser.add_argument("-i","--InputFiles",type=str,action='store', default="mc21", choices=['mc20','mc21'],help="fill in")

#too many things need to be changed along with this, leave out for now
#parser.add_argument("-t","--ThresholdNames",type=str,action='store', nargs="+", default=[],help="fill in")

args = parser.parse_args()

#ATLAS plotting style
ROOT.gROOT.LoadMacro("/eos/user/h/hrussell/Bphys_Data/AtlasStyle/AtlasStyle.C")
ROOT.gROOT.LoadMacro("/eos/user/h/hrussell/Bphys_Data/AtlasStyle/AtlasLabels.C")

ROOT.SetAtlasStyle()

ROOT.gStyle.SetErrorX(0.5)

#Tells root not to open canvases to screen
ROOT.gROOT.SetBatch(True)

#Reading the file using ROOT
beekj = None
beeke = None

if args.InputFiles == "mc20":

    beekj = ROOT.TFile.Open('/eos/user/h/hrussell/Bphys_Data/synced_files/valid1.300592.Bd_Kstar_Kpi_Jpsi_e4e4.r13670.beek_v8.root')
    beeke = ROOT.TFile.Open('/eos/user/h/hrussell/Bphys_Data/synced_files/valid1.300590.Bd_Kstar_Kpi_e4e4.r13670.beek_v8.root')
    
elif args.InputFiles == "mc21":

    beekj = ROOT.TFile.Open('/eos/user/h/hrussell/Bphys_Data/synced_files/mc21_13p6TeV.801874.Bd_Kstar_Kpi_Jpsi_e4e4.r14016.beek_v8.root')
    beeke = ROOT.TFile.Open('/eos/user/h/hrussell/Bphys_Data/synced_files/mc21_13p6TeV.801872.Bd_Kstar_Kpi_e4e4.r14016.beek_v8.root')

else:

    print("no files yet for",args.InputFiles)
    
mybeekj = beekj.Get("trig")
mybeeke = beeke.Get("trig")

print('There are ',mybeekj.GetEntries(),'events in J/psi Kstar')

output_dir = '/eos/user/s/salshama/BeekJpsi/Fall_Plots/'

if args.OutputFolder != '':
    output_dir = args.OutputFolder
#output_dir = '/eos/user/h/hrussell/Bphys_Data/AnalysisResults/run3L1/'

print('Putting plots into this directory:',output_dir)

plot_tag = args.OutputName

#Creating a TCanvas object
c1 = ROOT.TCanvas("c1","c1",800,600)

#Different combinations of permutations with requirements for None, Loose, Medium, Tight
all_64 = ['NNN', 'NNL', 'NNM', 'NNT', 'NLN', 'NLL', 'NLM', 'NLT', 'NMN', 'NML', 'NMM',\
'NMT', 'NTN', 'NTL', 'NTM', 'NTT', 'LNN', 'LNL', 'LNM', 'LNT', 'LLN', 'LLL', 'LLM',\
'LLT', 'LMN', 'LML', 'LMM', 'LMT', 'LTN', 'LTL', 'LTM', 'LTT', 'MNN', 'MNL', 'MNM',\
'MNT', 'MLN', 'MLL', 'MLM', 'MLT','MMN', 'MML', 'MMM', 'MMT', 'MTN', 'MTL', 'MTM',\
'MTT', 'TNN', 'TNL', 'TNM', 'TNT', 'TLN', 'TLL', 'TLM', 'TLT', 'TMN', 'TML', 'TMM',\
'TMT', 'TTN', 'TTL', 'TTM', 'TTT']

#Calling the function to get ROI information for resonant and non-resonant data

mybeekj_ROI_infos = BeekFunctions.fill_ROI_infos(mybeekj)
mybeeke_ROI_infos = BeekFunctions.fill_ROI_infos(mybeeke)

#Calling other functions to create and fill histograms and efficiencies

#th_names is the list of triggers that go into the "all triggers fidreco" plot 
#change this list depending on what you want to plot!!

th_names  = ['2eEM5_TTN_MMN', 'eEM7_2eEM5_TTN_MMN', '2eEM5','eEM7_2eEM5_MMN_LLN',\
'eEM7_2eEM5','eEM7_2eEM5_TTN_NNN','eEM7_2eEM5_TTN_TTN']

#Printing out the rates of the events

print(BeekFunctions.find_rate(th_names))

#sys.exit(0)

#if len(args.ThresholdNames)>0:
#   th_names = args.ThresholdNames
   
print("Using thresholds:",th_names)

eff_dicts_mass_res    = BeekFunctions.get_eff_hist_dict(th_names,'resmass',100,0,10,'Truth Dielectron Mass [GeV]')
eff_dicts_dR_res      = BeekFunctions.get_eff_hist_dict(th_names,'resdR',100,0,1,'Truth Dielectron dR')

eff_dicts_mass_nonres = BeekFunctions.get_eff_hist_dict(th_names,'nonresmass',100,0,10,'Truth Dielectron Mass [GeV]')
eff_dicts_dR_nonres   = BeekFunctions.get_eff_hist_dict(th_names,'nonresdR',100,0,1,'Truth Dielectron dR')

eff_dicts_mass_both   = BeekFunctions.get_eff_hist_dict(th_names,'bothmass',100,0,10,'Truth Dielectron Mass [GeV]')
eff_dicts_dR_both     = BeekFunctions.get_eff_hist_dict(th_names,'bothdR',100,0,1,'Truth Dielectron dR')

#Fill histograms
print("Filling histograms....")
print("(still?) Using thresholds:",th_names)

#Non-resonant done
BeekFunctions.fill_hists(th_names, eff_dicts_mass_nonres,eff_dicts_dR_nonres,mybeeke_ROI_infos,mybeeke)
print("...done nonres")

#Resonant done
BeekFunctions.fill_hists(th_names, eff_dicts_mass_res,eff_dicts_dR_res,mybeekj_ROI_infos,mybeekj)
print("...done res")

#Fill the "both" hists twice! One for each input file:

#For resonant
BeekFunctions.fill_hists(th_names, eff_dicts_mass_both,eff_dicts_dR_both,mybeeke_ROI_infos,mybeeke)
print("...done nonres into combined plots")

#And for non-resonant
BeekFunctions.fill_hists(th_names, eff_dicts_mass_both,eff_dicts_dR_both,mybeekj_ROI_infos,mybeekj)
print("...done res into combined plots")
    
BeekFunctions.calc_efficiencies(eff_dicts_mass_res)
BeekFunctions.calc_efficiencies(eff_dicts_dR_res)

BeekFunctions.calc_efficiencies(eff_dicts_mass_nonres)
BeekFunctions.calc_efficiencies(eff_dicts_dR_nonres)

BeekFunctions.calc_efficiencies(eff_dicts_mass_both)
BeekFunctions.calc_efficiencies(eff_dicts_dR_both)

#Do not forget to change the output name if you change the trigger list - or the file will be overwritten!

BeekFunctions.display_hists_oneSelection(c1, eff_dicts_mass_both,\
['2eEM5_TTN_MMN', 'eEM7_2eEM5_TTN_MMN', 'eEM7_2eEM5_TTN_TTN', 'eEM7_2eEM5_MMN_LLN',\
'eEM7_2eEM5','eEM7_2eEM5_TTN_NNN'], 'fidreco', 'both_mass_eEM7_2eEM5_TTNMMNLLNNNN', plot_tag)

BeekFunctions.display_hists_oneSelection(c1, eff_dicts_dR_both,\
['2eEM5_TTN_MMN', 'eEM7_2eEM5_TTN_MMN', 'eEM7_2eEM5_TTN_TTN', 'eEM7_2eEM5_MMN_LLN',\
'eEM7_2eEM5','eEM7_2eEM5_TTN_NNN'], 'fidreco', 'both_dR_eEM7_2eEM5_TTNMMNLLNNNN', plot_tag)

for ith,th_name in enumerate(th_names):

	BeekFunctions.display_hists_oneTrigger(c1, eff_dicts_mass_both[ith],'both_'+th_name+'_mass',plot_tag)
	BeekFunctions.display_hists_oneTrigger(c1, eff_dicts_mass_res[ith],'res_'+th_name+'_mass',plot_tag)
	BeekFunctions.display_hists_oneTrigger(c1, eff_dicts_mass_nonres[ith],'nonres_'+th_name+'_mass',plot_tag)
	
	BeekFunctions.display_hists_oneTrigger(c1, eff_dicts_dR_both[ith],'both_'+th_name+'_dR',plot_tag)
	BeekFunctions.display_hists_oneTrigger(c1, eff_dicts_dR_res[ith],'res_'+th_name+'_dR',plot_tag)
	BeekFunctions.display_hists_oneTrigger(c1, eff_dicts_dR_nonres[ith],'nonres_'+th_name+'_dR',plot_tag)